FROM alpine:latest AS base
# see https://gitlab.com/usvc/images/ci/base
RUN wget -O /usr/bin/alpine-bootstrap.sh https://gitlab.com/usvc/images/ci/base/raw/master/shared/alpine-bootstrap.sh \
  && chmod +x /usr/bin/alpine-bootstrap.sh \
  && /usr/bin/alpine-bootstrap.sh \
  && rm -rf /usr/bin/alpine-bootstrap.shared
# TODO: insert your custom stuff here
WORKDIR /
LABEL \
  description="description" \
  canonical_url="https://gitlab.com/usvc/images/???" \
  license="MIT" \
  maintainer="zephinzer" \
  authors="zephinzer"

FROM base AS docker
# see https://gitlab.com/usvc/images/ci/docker
RUN wget -O /usr/bin/docker-bootstrap.sh https://gitlab.com/usvc/images/ci/docker/raw/master/shared/docker-bootstrap.sh \
  && chmod +x /usr/bin/docker-bootstrap.sh \
  && /usr/bin/docker-bootstrap.sh \
  && rm -rf /usr/bin/docker-bootstrap.sh
VOLUME [ "/var/run/docker.sock" ]

FROM base AS gitlab
# see https://docs.gitlab.com/ee/ci/docker/using_docker_build.html
ENV DOCKER_HOST=tcp://docker:2375/ \
  DOCKER_DRIVER=overlay2
